# Base node for non-coding intergenic mutations. Extended by coding codingNode and aaNode
class mNode:
    def __init__(self, form, num1, contig, pos, mutation, freq, gene_pos, gene_prod):
        self.form = form
        self.num1 = num1
        self.contig = contig
        self.pos = pos
        self.mutation = mutation
        self.freq = freq
        self.gene_pos = gene_pos
        self.gene_prod = gene_prod
        self.count = 1

    def increment_count(self):
        self.count += 1

    def get_form(self):
        return self.form

    def get_num1(self):
        return self.num1

    def get_contig(self):
        return self.contig

    def get_pos(self):
        return str(self.pos)

    def get_mutation(self):
        return self.mutation

    def get_freq(self):
        return str(self.freq)

    def get_gen_pos(self):
        return self.gene_pos

    def get_gene_prod(self):
        return self.gene_prod

    def get_count(self):
        return "num_lines=" + str(self.count)

    def get_all(self):
        strings = [self.get_form(), self.get_num1(), self.get_contig(), self.get_pos(), self.get_mutation(),
                   self.get_freq(), self.get_gen_pos(), self.get_gene_prod(), self.get_count()]
        return strings
    def to_string(self):
        return self.get_all()


# Node for coding mutation that does not contain aa data. Inherits all of mNode with addition of gene_strand attribute
class cNode(mNode):
    def __init__(self, form, num1, contig, pos, mutation, freq, gene_pos, gene_prod, gene_strand):
        mNode.__init__(self, form, num1, contig, pos, mutation, freq, gene_pos, gene_prod)
        self.gene_strand = gene_strand
    def get_gene_strand(self):
        return self.gene_strand

    def to_string(self):
        strings = self.get_all()
        strings.append(self.get_gene_strand())
        return strings

class aaNode(cNode):
    #def __int__(self, form, num1, contig, pos, mutation, freq, gene_pos, gene_prod, gene_strand, aa_new_seq, aa_pos, aa_ref, codon_new_seq, codon_num, codon_pos, codon_ref, snp_type):
    def __init__(self, form, num1, contig, pos, mutation, freq, gene_pos, gene_prod, gene_strand):  #todo: figure out what is going on with overloading init
        cNode.__init__(self, form, num1, contig, pos, mutation, freq, gene_pos, gene_prod, gene_strand)

        # self.aa_new_seq = aa_new_seq
        # self.aa_pos = aa_pos
        # self.aa_ref = aa_ref
        # self.codon_new_seq = codon_new_seq
        # self.codon_num = codon_num
        # self.codon_pos = codon_pos
        # self.codon_ref = codon_ref
        # self.snp_tpye = snp_type

    def set_attributes(self, aa_new_seq, aa_pos, aa_ref, codon_new_seq, codon_num, codon_pos, codon_ref, snp_type):
        self.aa_new_seq = aa_new_seq
        self.aa_pos = aa_pos
        self.aa_ref = aa_ref
        self.codon_new_seq = codon_new_seq
        self.codon_num = codon_num
        self.codon_pos = codon_pos
        self.codon_ref = codon_ref
        self.snp_tpye = snp_type

    def get_aa_new_seq(self):
        return self.aa_new_seq

    def get_aa_pos(self):
        return self.aa_pos

    def get_aa_ref(self):
        return self.aa_ref

    def get_codon_new_seq(self):
        return self.codon_new_seq

    def get_codon_num(self):
        return self.codon_num

    def get_codon_pos(self):
        return self.codon_pos

    def get_codon_ref(self):
        return self.codon_ref

    def get_snp_type(self):
        return self.snp_tpye

    def to_string(self):
        strings = self.get_all()
        strings.append(self.get_gene_strand())
        strings.append(self.get_aa_new_seq())
        strings.append(self.get_aa_pos())
        strings.append(self.get_aa_ref())
        strings.append(self.get_codon_new_seq())
        strings.append(self.get_codon_num())
        strings.append(self.get_codon_pos())
        strings.append(self.get_codon_ref())
        strings.append(self.get_snp_type())
        return strings


# class aaNode(cNode):
#     def __int__(self, form, num1, contig, pos, mutation, freq, gene_pos, gene_prod, gene_strand, aa_new_seq, aa_pos, aa_ref, codon_new_seq, codon_num, codon_pos, codon_ref, snp_type):
#         cNode.__init__(self, form, num1, contig, pos, mutation, freq, gene_pos, gene_prod, gene_strand)
#         self.aa_new_seq = aa_new_seq
#         self.aa_pos = aa_pos
#         self.aa_ref = aa_ref
#         self.codon_new_seq = codon_new_seq
#         self.codon_num = codon_num
#         self.codon_pos = codon_pos
#         self.codon_ref = codon_ref
#         self.snp_tpye = snp_type
#
#     def get_aa_new_seq(self):
#         return self.aa_new_seq
#
#     def get_aa_pos(self):
#         return self.aa_pos
#
#     def get_aa_ref(self):
#         return self.aa_ref
#
#     def get_codon_new_seq(self):
#         return self.codon_new_seq
#
#     def get_codon_num(self):
#         return self.codon_num
#
#     def get_codon_pos(self):
#         return self.codon_pos
#
#     def get_codon_ref(self):
#         return self.codon_ref
#
#     def get_snp_type(self):
#         return self.snp_tpye
#
#     def to_string(self):
#         strings = self.get_all()
#         strings.append(self.get_gene_strand())
#         strings.append(self.get_aa_new_seq())
#         strings.append(self.get_aa_pos())
#         strings.append(self.get_aa_ref())
#         strings.append(self.get_codon_new_seq())
#         strings.append(self.get_codon_pos())
#         strings.append(self.get_codon_ref())
#         strings.append(self.get_snp_type())
#         return strings
