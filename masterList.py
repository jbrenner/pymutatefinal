import csv
from Nodes import *



class masterListClass:

    def __init__(self, input_file):
        self.master = []
        input = list(csv.reader(open(input_file, encoding='utf-8'), delimiter='\t'))
        # loop through all rows of list skip first 4 lines to strip header
        for r in range(4,len(input)):
            print(len(input[r])) #test
            if len(input[r])==12:
                self.master.append(self.get_mNode(input[r]))  # append mNode to master list
            elif len(input[r])==11:
               self.master.append(self.get_cNode(input[r]))  # append cNode to master list
            elif len(input[r]) == 20:
                self.master.append(self.get_aaNode(input[r]))  # append aaNode to master list


    def get_mNode(self, input):
            form = input[0]
            num1 = input[2]
            contig = input[3]
            pos = input[4]
            mutation = input[5]

            #loop to find attributes that appear in different cols depending on row
            for x in input[5:]: # loop through each col in a given row r
                col = str(x) # set col to a string to use find()
                if col.find("freq") != -1:
                    freq= col
                elif col.find("gene_position") != -1:
                    gene_pos = col
                elif col.find("gene_product") != -1:
                    gene_prod= col
            node = mNode(form, num1, contig, pos, mutation, freq, gene_pos, gene_prod)
            return node

    def get_cNode(self, input):
        form = input[0]
        num1 = input[2]
        contig = input[3]
        pos = input[4]
        mutation = input[5]

        #loop to find attributes that appear in different cols depending on row
        for x in input[5:]:  # loop through each col in a given row r
            col = str(x)  # set col to a string to use find()
            if col.find("freq") != -1:
                freq = col
            elif col.find("gene_position") != -1:
                gene_pos = col
            elif col.find("gene_product") != -1:
                gene_prod= col
            elif col.find("gene_strand") != -1:
                gene_strand = col
        node = cNode(form, num1, contig, pos, mutation, freq, gene_pos, gene_prod, gene_strand)
        return node

    def get_aaNode(self, input):
        form = input[0]
        num1 = input[2]
        contig = input[3]
        pos = input[4]
        mutation = input[5]

        #loop to find attributes that appear in different cols depending on row
        for x in input[5:]:  # loop through each col in a given row r
            col = str(x)  # set col to a string to use find()
            if col.find("freq") != -1:
                freq = col
            elif col.find("gene_position") != -1:
                gene_pos = col
            elif col.find("gene_product") != -1:
                gene_prod= col
            elif col.find("gene_strand") != -1:
                gene_strand = col
            elif col.find("aa_new_seq") != -1:
                aa_new_seq = col
            elif col.find("aa_position") != -1:
                aa_pos = col
            elif col.find("aa_ref_seq") != -1:
                aa_ref = col
            elif col.find("codon_new_seq") != -1:
                codon_new_seq = col
            elif col.find("codon_position") != -1:
                codon_pos = col
            elif col.find("codon_number") != -1:
                codon_num = col
            elif col.find("codon_ref_seq") != -1:
                codon_ref = col
            elif col.find("snp_type") != -1:
                snp_type = col

        node = aaNode(form, num1, contig, pos, mutation, freq, gene_pos, gene_prod, gene_strand) #todo: fix inheritence
        node.set_attributes(aa_new_seq, aa_pos, aa_ref, codon_new_seq, codon_num, codon_pos, codon_ref, snp_type) # todo: fix inheritence issues

        return node

    # Method for merging files from other lines into the list once it has been initialized with the first file
    def merge(self, input_file):
        input = list(csv.reader(open(input_file, encoding='utf-8'), delimiter='\t'))
        count = 0
        tempMaster = self.master
          # loop through all rows of list skip first 4 lines to strip header
        for r in range(4,len(input)):
            print(len(input[r])) #test
            if len(input[r])==12:
                newNode = self.get_mNode(input[r])
            elif len(input[r])==11:
                newNode = self.get_cNode(input[r])
            elif len(input[r]) == 20:
                newNode = self.get_aaNode(input[r])

            while count < len(self.master):
                if newNode.get_pos() < self.master[count].get_pos():
                    count += 1
                elif newNode.get_pos() == self.master[count].get_pos():
                    self.master[count].increment_count()
                    count +=1
                    break
                elif newNode.get_pos() > self.master[count].get_pos():
                    tempMaster.insert(count+1, newNode)
                    break
            if count > len(self.master):
                tempMaster.append(newNode)
        self.master = tempMaster



    def getResults(self):
        with open('results.csv', 'w', newline='\n') as fp:
            writer = csv.writer(fp, delimiter='\t')

            for node in self.master:
                print(node.get_count())
                writer.writerows([node.to_string()])















