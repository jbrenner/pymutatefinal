import unittest
from masterList import *


class testMasterList(unittest.TestCase):
    def testNodes(self):
        self.test = masterListClass("test_data.txt")

        self.mNode_input = ["INS","3538","contig00001","49014","ATCAAATTTGATGAAGTAAAAGCAATACGGTGACGCGGGGTGGAGCAGCCTGGTAGCTCGTCGGGCTCATAACCCGAAGGTCGTCGGTTCAAATCCGGCCCCCGCAACCA","frequency=0.4133",	"gene_position=intergenic (–/–)", "gene_product=–/–","num_lines=1"]
        self.assertEqual(len(self.test.master[1].to_string()), len(self.mNode_input), "Incorrect size of node")
        self.assertEqual(self.test.master[1].to_string(), self.mNode_input, "Input does not match node")
        self.assertTrue(isinstance(self.test.master[1], mNode), "Not correct node type")

        self.cNode_input = ["INS","817","contig00003","194578","G","frequency=0.0470","gene_position=coding (80/141 nt)","gene_product=FIG00643378: hypothetical protein","num_lines=1","gene_strand=>"]
        self.assertEqual(len(self.test.master[2].to_string()), len(self.cNode_input), "Incorrect size of node")
        self.assertEqual(self.test.master[2].to_string(), self.cNode_input, "Input does not match node")
        self.assertTrue(isinstance(self.test.master[2], cNode), "Not correct node type")

        self.aaNode_input = ["SNP","648","contig00106","429","A", "frequency=1", "gene_position=367",  "gene_product=IS5 transposase", "num_lines=1", "gene_strand=>", "aa_new_seq=S","aa_position=123","aa_ref_seq=G","codon_new_seq=AGT", "codon_number=123", "codon_position=1", "codon_ref_seq=GGT", "snp_type=nonsynonymous"]
        self.assertEqual(len(self.test.master[0].to_string()), len(self.aaNode_input), "Incorrect size of node")
        self.assertEqual(self.test.master[0].to_string(), self.aaNode_input, "Input does not match node")
        self.assertTrue(isinstance(self.test.master[0], aaNode), "Not correct node type")

    def testMerge(self):
        self.test = masterListClass("test_data.txt")
        self.test.merge("test_data2.txt")
        self.test.getResults()

        self.assertEqual(len(self.test.master), 4, "Master incorrect length")
        self.assertEqual(self.test.master[0].get_pos(),"429", "Positions do not match")
        self.assertEqual(self.test.master[1].get_pos(),"49014", "Positions do not match")
        self.assertEqual(self.test.master[2].get_pos(),"49015", "Positions do not match")
        self.assertEqual(self.test.master[3].get_pos(),"194578", "Positions do not match")

        self.assertEqual(self.test.master[0].get_count(),'num_lines=2', "Incorrect count")
        self.assertEqual(self.test.master[1].get_count(),'num_lines=1', "Incorrect count")
        self.assertEqual(self.test.master[1].get_count(),'num_lines=1', "Incorrect count")
        self.assertEqual(self.test.master[3].get_count(),'num_lines=2', "Incorrect count")









